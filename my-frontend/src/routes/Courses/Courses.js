import React from "react";
import classes from "../../style.module.scss";

const Courses = () => {
    return (
        <div className={classes.main}>
            <div className={classes.main__navigation}>Navbar</div>
            <div className={classes.main__content}>Content</div>
        </div>
    )
}
export default Courses