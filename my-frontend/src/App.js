import './App.css';
import { Route, Routes } from 'react-router-dom';
import General from "./routes/General/General";
import Courses from "./routes/Courses/Courses";
import Shop from "./routes/Shop/Shop";
import News from "./routes/News/News";

function App() {
  return (
      <Routes>
          <Route path="/" exact element={<General />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/shop" element={<Shop />} />
          <Route path="/news" element={<News />} />
      </Routes>
  );
}

export default App;
